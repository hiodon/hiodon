Title: 数学环境测试
Date: 2021-01-09
Category: Hack
Tags: Mathjax
Slug: Some_formulas


# Hello Pelican!

inline $\pi$

This site is hosted on GitLab Pages!

$$
a^2 + b ^2 = c^2
$$

$$
\begin{align}
\label{eq:1}
  x^2 - y^2    
\end{align}
$$

cite equation ( $\ref{eq:1}$ )

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
