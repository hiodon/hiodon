Title: 本站备忘
Date: 2020-11-05
Category: Hack
Tags: pelican, gitlab, godaddy
Slug: hiodon_notes

# Intro

搭这个新的 blog 也踩了一些坑, 在做一备忘.

# Pelican

参考说明文档: [Pelican
docs](https://docs.getpelican.com/en/stable/quickstart.html) , 安装 Pelican, 并
quickstart 一个项目. 

# GitLab

将项目 git 初始化, push 到 gitlab 上. 

与 Github 不同的是, 要想使项目在 gitlab 上
正确地布置为 gitlab pages, 需要配置 GitLab CI. gitlab 提供了许多模板, 可以直接使
用 html 的模板, 但是要略加改动. 因为 Pelican 默认将 html 文件输出到 `output` 文
件夹, 所以最终的 CI 配置为
```
# This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/plain-html
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r output/* .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```

# Domain

自定义域名为可选操作.

在 godaddy 上购买了 hiodon.com , 因此用为本站域名.

在 gitlab 仓储的 settings -> pages 中按说明添加新的 domain 即可. 

解析时遇到了一个问题, DNS 中有一条无法删除的 A 记录. 经过咨询客服, 得知在 DNS 设
置下方有一条转址, 将那个删除, 就可以编辑了. 客服还是很专业的, 一下就解决了问题.
有问题打电话给客服也是不错的选择. 

咨询客服的意外收获, 不带 www 的网址, 要用 A 指向 IP 才可以, 不能用 CNAME 指向
远程, 不然大陆无法解析. gitlab pages 的 IP 可以在 gitlab pages 的 docs 中查到.

# FreeSSL

添加 SSL 证书是可选操作.

添加 SSL 说明书后, 网址变为 https 前缀, 向访问者证明你的网站是安全的. 

SSL 证书可以买, 也可以在 [fressl.cn](https://freessl.cn/) 免费生成. 生成后, 直接
在 gitlab 仓储的 settings -> pages 中勾选 `Force HTTPS` 即可.
