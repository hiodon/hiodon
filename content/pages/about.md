Title: About
Slug: about

# Why hiodon?

书和电影看过就忘. 写读书笔记或许会改善这种情况.

又搬过来一些以前的日记. 以后闲了还会继续搬.

只是写给自己看

# Why the name "hiodon"?

随便找了一个动物名字, 并且此域名便宜. [Hiodon(月眼魚屬)](https://zh.wikipedia.org/wiki/%E8%83%8C%E7%94%B2%E6%9C%88%E7%9C%BC%E9%AD%9A)

# Contact me

email: phyer219@gmail.com :)
